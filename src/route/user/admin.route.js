const express = require('express')
const router = express.Router()
const { ValidateUserCreate,ValidateSignin } = require('../../validator/user.validator')
const { IsRequestValidated } = require('../../../service-share/validator/common-validator')
const { CreateAdmin,SignIn,hookLoginGoogle,signout } = require('../../controller/user/admin.controller')
const { requireSignin } = require('../../../service-share/middle-ware/token.middle-ware')

router.post("/admin/create", ValidateUserCreate,IsRequestValidated, CreateAdmin)
router.post("/admin/login", ValidateSignin,IsRequestValidated, SignIn)
router.post('/admin/hook-auth-google', hookLoginGoogle)
router.post('/admin/signout', requireSignin, signout)


module.exports = router
