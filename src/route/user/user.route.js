const express = require('express')
const router = express.Router()
const { CreateUser, SignIn, hookLoginGoogle, signout} = require('../../controller/user/user.controller')
const { ValidateUserCreate,ValidateSignin } = require('../../validator/user.validator')
const { IsRequestValidated } = require('../../../service-share/validator/common-validator')
const { requireSignin } = require('../../../service-share/middle-ware/token.middle-ware')

router.post('/user/create', ValidateUserCreate,IsRequestValidated, CreateUser)
router.post('/user/login', ValidateSignin,IsRequestValidated, SignIn)
router.post('/user/hook-auth-google', hookLoginGoogle)
router.post('/user/signout', requireSignin, signout)

module.exports = router
