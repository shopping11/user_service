const bcrypt = require('bcryptjs')
const { ErrorHandler, asyncHandler, AppResponseError } = require('../../../service-share/middle-ware/response.middle-ware')
const UserService = require('../../service/user.service')
const jwt = require('jsonwebtoken')
const { OAuth2Client } = require('google-auth-library');
const { UserRolesEnum, RegisterType } = require('../../model/user/user.enum');
const client = new OAuth2Client(process.env.ADMIN_GOOGLE_CLIENT_ID)
const { APP_BAD_REQUEST } = require("../../../service-share/const/bad-request.const");

exports.CreateAdmin = asyncHandler(async (req, res, next) => {
    const { password } = req.body
    req.body.role = UserRolesEnum.ADMIN
    req.body.password = bcrypt.hashSync(password, 10)
    const docs = await UserService.CreateUser(req)
    
  return  docs
})
  
exports.SignIn = asyncHandler(async (req, res) => {
  const { username, password } = req.body
  const user = await UserService.FindUserName( username )
  if (user?.registerFrom === RegisterType.OWN ) {
    if (user && user.authenticate(password) && user.role == UserRolesEnum.ADMIN) {
      const { _id, username, firstName, lastName, email, role } = user
      const token = jwt.sign({
        _id: _id,
        username: username,
        firstName: firstName,
        lastName: lastName,
        email: email,
        role: role
      }
        , process.env.JWT_SECRET, { expiresIn: '100d' })
      return {
        token: token,
        user: {
                username: username,
                firstName: firstName,
                lastName: lastName,
                email: email,
                role: role
              }
      }
      }
    }
    throw new AppResponseError(400,APP_BAD_REQUEST.LOGIN_FAILURE)
})

function GmailBuildResponse(docs) {
  const token = jwt.sign({
    _id: docs._id,
    username: docs.username,
    firstName: docs.firstName,
    lastName: docs.lastName,
    email:docs.email,
    role: docs.role
  }
    , process.env.JWT_SECRET, { expiresIn: '100d' });
  const user = {
    username: docs.username,
    firstName: docs.firstName,
    lastName: docs.lastName,
    email:docs.email,
    role: docs.role
  }
  return {token:token,user:user}
}

async function AdminGoogle(userDosc){
  const { given_name, family_name, email } = userDosc
  const user = await UserService.FindUserName( email )

  if (user && user.registerFrom === RegisterType.GOOGLE && user.role === UserRolesEnum.ADMIN ) {
    return GmailBuildResponse(user)
  }
  else if (user) {
    throw new AppResponseError(400,APP_BAD_REQUEST.EMAIL_DUPLICATE)
  }

  const docs = await UserService.CreateUserGoole({
    username: email,
    firstName: given_name,
    lastName: family_name,
    email: email,
    registerFrom: RegisterType.GOOGLE,
    role: UserRolesEnum.ADMIN
  })
  return GmailBuildResponse(docs)
}

exports.hookLoginGoogle = asyncHandler(async (req, res) => {
  const { tokenId } = req.body
  if (!tokenId) {
    throw new ErrorHandler(400,"can connect google")
  }
  const googleUser = await client.verifyIdToken({ idToken: tokenId, audience: process.env.ADMIN_GOOGLE_CLIENT_ID })
 
  const docs = await AdminGoogle(googleUser.payload)

  return docs
})

exports.signout = asyncHandler(async(req, res) => {
  await res.clearCookie('token')

  return 'Signout successfully...!'
});