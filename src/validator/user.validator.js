const { check } = require('express-validator')
const { ValidateExistError } = require('../../service-share/validator/validateExist.validator')
const { APP_BAD_REQUEST } = require("../../service-share/const/bad-request.const")
exports.ValidateUserCreate = [
  check('firstName')
    .notEmpty()
    .withMessage('required'),
  
  check('lastName')
    .notEmpty()
    .withMessage('required'),
  
  check('email')
    .notEmpty()
    .withMessage('required')
    .bail()
    .isEmail()
    .withMessage('invalid email')
    .bail()
    .custom((value) => {
      return ValidateExistError('User', { email: value }, APP_BAD_REQUEST.EMAIL_DUPLICATE)
    }),
  
  check('username')
    .notEmpty()
    .withMessage('required')
    .bail()
    .isLength({ min: 6 })
    .withMessage('at least 6 characters')
    .bail()
    .custom((value) => {
        return ValidateExistError('User', { username: value }, APP_BAD_REQUEST.USER_DUPLICATE)
    }),
  
  check('password')
    .notEmpty()
    .withMessage('required')
    .bail()
    .isLength({ min: 6 })
    .withMessage('at least 6 characters'),
  
  check('confirmPassword')
  .notEmpty()
  .withMessage('required')
  .bail()
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error('รหัสผ่านทั้งสองไม่ตรงกัน');
      }
      return true
    }),
  check('contactNumber')
    .notEmpty()
    .withMessage('required')
    .bail()
    .isMobilePhone()
    .withMessage('invalid phone number')
  
];

exports.ValidateSignin = [
  check('username')
    .notEmpty()
    .withMessage('required'),
  check('password')
    .notEmpty()
    .withMessage('required'),
];
