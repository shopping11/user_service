exports.UserRolesEnum = {
    USER: 'user',
    ADMIN: 'admin'
}

exports.RegisterType = {
    OWN: 'own',
    GOOGLE: 'google'
}