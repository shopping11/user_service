const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const encryptPlugin = require('../../../service-share/plugin/encrypt.plugin')
const { UserRolesEnum, RegisterType } = require('./user.enum')


const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        trim: true,
        min: 3,
        max:20
    },
    lastName : {
        type: String,
        required: true,
        trim: true,
        min: 3,
        max:20
    },
    username: {
        type: String,
        required: true,
        trim: true,
        unique: true,
        index: true,
        lowercase:true
    },
    email: {
        type: String,
        requried: true,
        trim: true,
        unique: true,
        lowercase:true
    },
    password: {
        type: String
    },
    role: {
        type: String,
        enum: [UserRolesEnum.USER, UserRolesEnum.ADMIN],
        default: UserRolesEnum.USER
    },
    contactNumber: {
        type:String
    },
    profilePicture: {
        tpye:String
    },
    registerFrom: {
        type: String,
        enum: [ RegisterType.OWN, RegisterType.GOOGLE ],
        default: RegisterType.OWN
    }
}, { timestamps: true })



userSchema.methods = {
    authenticate: function (password) { 
        return bcrypt.compareSync(password, this.password)
    }
}

userSchema.plugin(encryptPlugin,['contactNumber'])



module.exports = mongoose.model('User', userSchema)


