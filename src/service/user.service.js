const UserModel = require('../model/user/user.schema')

exports.CreateUser = (req) => {
    const docs = new UserModel(req.body)
    
     return docs.save()
}


exports.CreateUserGoole = (username, firstName, lastName, email, registerFrom, role) => { 
    const docs = new UserModel(username, firstName, lastName, email, registerFrom, role)

     return docs.save()
}

exports.FindUserName = async (username) => {
    const docs = await UserModel.findOne({username: username})
    return docs
}

// exports.find = async (condition) => {
//     const docs = await UserModel.find(condition)
//     return docs
// }

// exports.findOne = async (condition) => {
//     const docs = await UserModel.findOne(condition)
//     return docs
// }