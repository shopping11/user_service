const express = require('express')
const app = express()
const { responseHandle } = require('./service-share/middle-ware/response.middle-ware')
const mongooseConnection = require("./src/mongoose/mongoose.connection")
const morgan = require('morgan')
const cors = require('cors')
require('dotenv').config()
const userRoute = require('./src/route/user/user.route')
const adminRoute = require('./src/route/user/admin.route')

//connect mongoose
mongooseConnection()

//middleware
app.use(express.json())
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(morgan('dev'))

// route
app.use('/users', userRoute, adminRoute)

//middleware hook response
app.use((data, req, res, next) => {
  responseHandle(data,req,res,next)
})

app.listen(process.env.PORT, () => {
  console.log("server is running on port", process.env.PORT)
});