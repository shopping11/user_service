const { exec } = require("child_process");

exec("git submodule add -f https://gitlab.com/shopping11/service-share.git", (error, stdout, stderr) => {
  console.log(error);
  console.log(stdout);
  console.log(stderr);
})

exec("git submodule init", (error, stdout, stderr) => {
  console.log(error);
  console.log(stdout);
  console.log(stderr);
})

exec("git submodule update --recursive --init", (error, stdout, stderr) => {
  console.log(error);
  console.log(stdout);
  console.log(stderr);
})
